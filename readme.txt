=== Baseline WordPress Theme ===
Contributors: array
Donate link: https://arraythemes.com/
Tags: light, white, two-columns, fluid-layout, responsive-layout, custom-background, custom-colors, custom-menu, editor-style, featured-images, theme-options, translation-ready, blog, photography, clean, minimal, modern, design, art, portfolio, simple, contemporary, infinite-scroll, site-logo
Requires at least: 3.8
Tested up to: 4.5.1
Stable tag: 1.1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Baseline is an elegant, magazine-style theme that lets you quickly and easily create a beautiful site with any content. Baseline includes one, two and three column layouts, a customizable logo and header, finely-crafted typography and a snappy featured content drawer.

== Description ==

Baseline is an elegant, magazine-style theme that lets you quickly and easily create a beautiful site with any content. Baseline includes one, two and three column layouts, a customizable logo and header, finely-crafted typography and a snappy featured content drawer.

== Installation ==

1. Sign into your WordPress dashboard, go to Appearance > Themes, and click Add New.
2. Click Add New.
3. Click Upload.
4. Click Choose File and select the theme zip file you downloaded from Array.
5. Click Install Now.
6. After WordPress installs the theme, click Activate.
7. You've successfully installed your new theme! Head to Appearance > Getting Started to begin setting up your theme.

== Frequently Asked Questions ==

= I need help! What should I do? =

To get help with your theme, please log into your Dashboard at Array and click the Support tab.

== Screenshots ==

1. The theme home page filled with demo content.

== Change Log ==

= 1.1.0 – 8/22/16 =
* Added improvements for RTL users.
* Files modified: style.css, inc/sass/style.scss, rtl.css, readme.txt

= 1.0.9 – 7/8/16 =
* Minor improvements and file cleanup for ThemeForest review.
* Files modified: style.css, inc/sass/style.scss, readme.txt, footer.php, functions.php, header.php, inc/customizer.php, js/baseline.js, js/customizer.js, languages/baseline.pot, page.php, search.php, template-parts/content-featured-content.php, template-parts/content-none.php.

= 1.0.8 – 7/7/16 =
* Fixed featured content navigation bug in Firefox.
* Files modified: style.css, inc/sass/style.scss, readme.txt, template-parts/content-featured-item.php, js/baseline.js

= 1.0.7 – 7/5/16 =
* Added customizer option for adjusting the featured content excerpt length.
* Files modified: style.css, inc/sass/style.scss, readme.txt, customizer.php, template-parts/content-featured-item.php

= 1.0.6 – 6/30/16 =
* Fixed display of titles on archive and search pages.
* Files modified: style.css, inc/sass/style.scss, readme.txt

= 1.0.5 – 5/25/16 =
* Added check for featured post ID on archive links to prevent error.
* Added check for sidebar widgets.
* Files modified: template-parts/content-featured-item.php, style.css, inc/sass/style.scss, readme.txt, functions.php

= 1.0.4 – 5/17/16 =
* Updated FontAwesome to support newest icons.
* Added Snapchat support to social icon menu.

= 1.0.3 – 5/16/16 =
* Fixed issue where full-size images were being loaded on the grid. It's suggested that you install the Regenerate Thumbnails plugin after this update to refresh your images. Once the plugin is installed and activated, go to Tools > Regen. Thumbnails to refresh your images.

= 1.0.2 – 5/10/16 =
* Fixed prefix on several functions.

= 1.0.1 – 5/3/16 =
* Cleaned up customizer.php.

= 1.0 – 2/20/16 =
* Initial release.